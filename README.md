# [frontend-assignment](https://bitbucket.org/tamtam-nl/dtnl-dept-frontend-setup-assignment/)

---

# Table of Contents

1.  [Intro](#markdown-header-intro)
2.  [Install](#markdown-header-install)
3.  [Design](#markdown-header-design)

---

# Intro

Welcome to the readme of the Dept Frontend Setup Assignment.

This is a basic setup for creating (static) html templates.
Filled with automated tasks and configuration options.
It enables you to quickly & easily setup your project and get it running in no time.
Many things are already predefined, added and sorted out for you to take away some hassle.
The pro users can dive into settings an tasks, but that's not required to work with it.

# Install

Use the setup following these commands.

**When using nvm make sure this is not installed through brew, because this can result in errors when running npm scripts.**

**1. Install all the npm modules**

`npm install`

`cd sample-node-api`

`npm install`

**2. Start the project**

IN PROJECT ROOT

in a seperate console

`npm run api` (should not be closed)

and

`npm run gulp` or `npm run start`

# Design

![Design-1](https://bitbucket.org/tamtam-nl/dtnl-dept-frontend-setup-assignment/raw/ff16604566f5e61c555e1ea0d91fa1c35ddf0585/_design/1-modal-open.png)

![Design-2](https://bitbucket.org/tamtam-nl/dtnl-dept-frontend-setup-assignment/raw/ff16604566f5e61c555e1ea0d91fa1c35ddf0585/_design/2-form.png)

![Design-3](https://bitbucket.org/tamtam-nl/dtnl-dept-frontend-setup-assignment/raw/ff16604566f5e61c555e1ea0d91fa1c35ddf0585/_design/3-error-message.png)

![Design-4](https://bitbucket.org/tamtam-nl/dtnl-dept-frontend-setup-assignment/raw/ff16604566f5e61c555e1ea0d91fa1c35ddf0585/_design/4-success-toast.png)

# dtn-frontend
